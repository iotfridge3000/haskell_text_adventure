

data NPC = NPC { name :: String
               , hp :: Int
               , demeanor :: String
} deriving (Show)

data MapNode = MapNode { nodeNum :: Int
                       , entities :: [NPC]
                       , description :: String
} deriving (Show)

main = do
    let actions = [ "move", "talk", "inventory", "quit"]

    userInput <- getLine
    let playerAction = head (words userInput)
    if playerAction `elem` actions
        then putStrLn "That is a valid action"
        else
            putStrLn "You can't do that."

