
The goal with this is to see how far I can get with making a text adventure game in Haskell.

My main resources are the following:
Real World Haskell
Learn You A Haskell

By the time I'm "done", I'll compare my approach with the way Phil structured his PureScript game in his LeanPub book: https://leanpub.com/purescript/read

The project is a bit of a mess right now but I'm trying to get something up and running quickly before deciding to pivot over to using the Stack package manager.

TODO:

- Fix repo name later on when converting this to a Haskell Stack project 
